<?php

namespace CloudZentral\Tools\Providers;

use CloudZentral\Tools\Commands\InstallComposerPackages;
use CloudZentral\Tools\Commands\PullGit;
use CloudZentral\Tools\Commands\ReloadSupervisor;
use CloudZentral\Tools\Commands\RestartSupervisor;
use CloudZentral\Tools\Commands\StartSupervisor;
use CloudZentral\Tools\Commands\StopSupervisor;
use CloudZentral\Tools\Commands\Update;

/**
 * Class ServiceProvider
 * @package CloudZentral\Tools\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $supervisorEnabled = config('laravel-tools.supervisor_enabled', false);
        $commands = [
            PullGit::class,
            InstallComposerPackages::class,
            Update::class
        ];
        if($supervisorEnabled) {
            $commands = array_merge($commands, [
                StartSupervisor::class,
                StopSupervisor::class,
                RestartSupervisor::class,
                ReloadSupervisor::class
            ]);
        }

        if($this->app->runningInConsole()) {
            $this->commands($commands);
        }

        $this->publishes([__DIR__."/../Configs/laravel-tools.php" => config_path('laravel-tools.php')], 'config');
    }
}
