<?php

namespace CloudZentral\Tools\Commands;

use Illuminate\Console\Command;

/**
 * Class Update
 * @package CloudZentral\Tools\Commands
 */
class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Updating project...");
        $this->call("git:pull");
        $this->call("composer:install");
        if(config('laravel-tools.cache_config', true)) {
            $this->call("config:cache");
        }
        if(config('laravel-tools.supervisor_enabled', false)) {
            $this->call("supervisor:restart");
        }
        $this->info("Done");
        return true;
    }
}
