<?php

namespace CloudZentral\Tools\Commands;

use Illuminate\Console\Command;

/**
 * Class PullGit
 * @package CloudZentral\Tools\Commands
 */
class PullGit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull git changes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('git pull');
        return true;
    }
}
