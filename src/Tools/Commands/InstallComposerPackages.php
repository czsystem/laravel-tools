<?php

namespace CloudZentral\Tools\Commands;

use Illuminate\Console\Command;

/**
 * Class InstallComposerPackages
 * @package CloudZentral\Tools\Commands
 */
class InstallComposerPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'composer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install/update composer packages from lock file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('composer install');
        return true;
    }
}
