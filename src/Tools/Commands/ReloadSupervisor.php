<?php

namespace CloudZentral\Tools\Commands;

use Illuminate\Console\Command;

/**
 * Class ReloadSupervisor
 * @package CloudZentral\Tools\Commands
 */
class ReloadSupervisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'supervisor:reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload supervisor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('systemctl reload supervisor');
        return true;
    }
}
