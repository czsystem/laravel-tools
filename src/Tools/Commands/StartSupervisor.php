<?php

namespace CloudZentral\Tools\Commands;

use Illuminate\Console\Command;

/**
 * Class StartSupervisor
 * @package CloudZentral\Tools\Commands
 */
class StartSupervisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'supervisor:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start supervisor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        exec('systemctl start supervisor');
        return true;
    }
}
